#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import argparse
import datetime
import flask
import json
import logging
import os
import requests
import signal
import sys
import time
import hashlib

import cherrypy

from flask import Flask, jsonify,  Response, redirect, request, g, session

from flask_pyoidc import OIDCAuthentication
from flask_pyoidc.provider_configuration import ProviderConfiguration, ClientMetadata, ProviderMetadata
from flask_pyoidc.user_session import UserSession
from requests.exceptions import ReadTimeout
from flask_cors import CORS

app = Flask(__name__)


# add CORS headers
CORS(app, resources={r'/api/*': {'origins':os.getenv('BENCHSUITE_CORS_ALLOWED_ORIGINS', '')}})

# See http://flask.pocoo.org/docs/0.12/config/
app.config.update({
                    'OIDC_REDIRECT_URI': os.getenv('OIDC_REDIRECT_URI'),
                    'SECRET_KEY': os.getenv('BENCHSUITE_DEV_KEY'),
                    'PERMANENT_SESSION_LIFETIME': datetime.timedelta(days=7).total_seconds(),
                    'DEBUG': True})


ISSUER = os.getenv('OIDC_ISSUER_URL')
JWKS_URL = os.getenv('OIDC_JWKS_URL')
AUTHZ_URL = os.getenv('OIDC_AUTHZ_URL')
TOKEN_URL = os.getenv('OIDC_TOKEN_URL')
USERINFO_URL = os.getenv('OIDC_USERINFO_URL')
ENDSESSION_URL = os.getenv("OIDC_ENDSESSION_URL")
POST_LOGOUT_REDIRECT_URL = os.getenv("OIDC_POST_LOGOUT_REDIRECT_URL")
CLIENT = os.getenv('OIDC_CLIENT_NAME')

PROVIDER_NAME1 = 'provider1'
PROVIDER_METADATA = ProviderMetadata(issuer=ISSUER,
                                     jwks_uri=JWKS_URL,
                                     authorization_endpoint=AUTHZ_URL,
                                     token_endpoint=TOKEN_URL,
                                     end_session_endpoint=ENDSESSION_URL,
                                     userinfo_endpoint=USERINFO_URL)
PROVIDER_CONFIG1 = ProviderConfiguration(provider_metadata=PROVIDER_METADATA,
                                         client_metadata=ClientMetadata(CLIENT, os.getenv('BENCHSUITE_OIDC_CLIENT_SECRET'), post_logout_redirect_uris=[POST_LOGOUT_REDIRECT_URL]))

auth = OIDCAuthentication({PROVIDER_NAME1: PROVIDER_CONFIG1})


rest_base_url = ""
authn_redirect_url = ""

# To avoid suggestions in 404 replies
app.config['ERROR_404_HELP'] = False




def on_exit(sig, func=None):
    print('Bye bye...')
    sys.exit(1)

############# AUTHENTICATION #############

@app.route('/api/v3/login')
@auth.oidc_auth(PROVIDER_NAME1)
def login():
    response = redirect(os.getenv("AUTHN_REDIRECT_URL"))
    return response

@app.route('/api/v3/logout')
@auth.oidc_logout
@auth.oidc_auth(PROVIDER_NAME1)
def logout():
    # this code is never executed
    return None

@app.route('/api/v3/postlogout')
def postlogout():
    print("REDIRECTING...")
    response = redirect(os.getenv("AUTHN_REDIRECT_URL"))
    return response

def _getLoggedUser():
    user_session = UserSession(flask.session, PROVIDER_NAME1)
    if user_session and user_session.userinfo and "email" in user_session.userinfo:
        logging.info("Logged user is " + user_session.userinfo["preferred_username"])
        return user_session.userinfo["preferred_username"]
    else:
        logging.info("User is anonymous")
        return None

def _getUserOrganization():
    user_session = UserSession(flask.session, PROVIDER_NAME1)
    logging.debug(user_session.userinfo)
    if user_session and user_session.userinfo and "organizations" in user_session.userinfo:
        organizations = user_session.userinfo["organizations"]
        if len(organizations)>0:
            org = user_session.userinfo["organizations"][0]
            if org.startswith("/"):
                org = org[1:]
            return org
        else:
            logging.info("User belongs to no organization")
            return None
    else:
        logging.info("User belongs to no organization")
        return None

############# PROXYING UTILS #############

def _authHeader():
    headers = {}
    
    user = _getLoggedUser()
    if user:
        headers["benchsuite-user"] = user
    
    org = _getUserOrganization()
    if org:
        headers["benchsuite-user-org"] = org
    
    return headers

def _genericGet(url, params={}):
    response = requests.get(url, headers=_authHeader(), params=params)
    return Response(response.text, mimetype='application/json', status=response.status_code)

def _genericDelete(url):
    response = requests.delete(url, headers=_authHeader())
    return Response(response.text, mimetype='application/json', status=response.status_code)

def _genericPost(url, payload):
    payload = json.loads(payload.decode("utf-8"))
    response = requests.post(url, json=payload, headers=_authHeader())
    return Response(response.text, mimetype='application/json', status=response.status_code)

def _genericPut(url, payload):
    payload = json.loads(payload.decode("utf-8"))
    response = requests.put(url, json=payload, headers=_authHeader())
    return Response(response.text, mimetype='application/json', status=response.status_code)

def _genericPatch(url, payload):
    payload = json.loads(payload.decode("utf-8"))
    response = requests.patch(url, json=payload, headers=_authHeader())
    return Response(response.text, mimetype='application/json', status=response.status_code)

def _getRestBaseUrl():
    return os.getenv("BENCHSUITE_REST_BASE_URL")

################## PROVIDER OPERATIONS ###################

@app.route('/api/v3/providers/types', methods=['GET'])
def listProviderTypes():
    return _genericGet(_getRestBaseUrl()+"/providers/types")

@app.route('/api/v3/providers', methods=['GET'])
def listProviders():
    return _genericGet(_getRestBaseUrl()+"/providers")


@app.route('/api/v3/providers', methods=['POST'])
def addProvider():
    return _genericPost(_getRestBaseUrl()+"/providers/", request.data)

# exposed method (passing the authenticated user, if any)
@app.route('/api/v3/providers/<provider_id>')
def getProvider(provider_id):
    return _genericGet(_getRestBaseUrl()+"/providers/"+provider_id)

# exposed method (passing the authenticated user, if any)
@app.route('/api/v3/providers/<provider_id>', methods=['DELETE'])
def deleteProvider(provider_id):
    return _genericDelete(_getRestBaseUrl()+"/providers/"+provider_id)

@app.route('/api/v3/providers/<provider_id>', methods=['PUT'])
def updateProvider(provider_id):
    return _genericPut(_getRestBaseUrl()+"/providers/"+provider_id, request.data)

@app.route('/api/v3/providers/<provider_id>', methods=['PATCH'])
def patchProvider(provider_id):
    return _genericPatch(_getRestBaseUrl()+"/providers/"+provider_id, request.data)

@app.route('/api/v3/providers/<provider_id>/vm_images')
def getProviderImages(provider_id):
    return _genericGet(_getRestBaseUrl()+"/providers/"+provider_id+"/vm_images")

@app.route('/api/v3/providers/<provider_id>/vm_sizes')
def getProviderSizes(provider_id):
    return _genericGet(_getRestBaseUrl()+"/providers/"+provider_id+"/vm_sizes")

@app.route('/api/v3/providers/<provider_id>/check')
def getProviderCheckConnection(provider_id):
    return _genericGet(_getRestBaseUrl()+"/providers/"+provider_id+"/check")

@app.route('/api/v3/providers/<provider_id>/vm/params')
def getProviderVMParams(provider_id):
    return _genericGet(_getRestBaseUrl()+"/providers/"+provider_id+"/vm/params")

@app.route('/api/v3/providers/<provider_id>/vm/images')
def getProviderVMImages(provider_id):
    return _genericGet(_getRestBaseUrl()+"/providers/"+provider_id+"/vm/images")

@app.route('/api/v3/providers/<provider_id>/vm/flavours')
def getProviderVMFlavours(provider_id):
    return _genericGet(_getRestBaseUrl()+"/providers/"+provider_id+"/vm/flavours")

@app.route('/api/v3/providers/<provider_id>/user_actions')
def getProviderActions(provider_id):
    return _genericGet(_getRestBaseUrl()+"/providers/"+provider_id+"/user_actions")

################## EXECUTIONS OPERATIONS ###################

@app.route('/api/v3/executions', methods=['GET'])
def listExecutions():
    return _genericGet(_getRestBaseUrl()+"/executions")

@app.route('/api/v3/executions/<execution_id>', methods=['GET'])
def getExecution(execution_id):
    return _genericGet(_getRestBaseUrl()+"/executions/"+execution_id)

@app.route('/api/v3/executions/<execution_id>/request', methods=['GET'])
def getExecutionRequest(execution_id):
    return _genericGet(_getRestBaseUrl()+"/executions/"+execution_id+"/request")

@app.route('/api/v3/executions/<execution_id>/runs', methods=['GET'])
def getExecutionLogs(execution_id):
    return _genericGet(_getRestBaseUrl()+"/executions/"+execution_id+"/runs")

################## WORKLOADS OPERATIONS ###################

@app.route('/api/v3/workloads/', methods=['GET'])
def listWorkloads():
    return _genericGet(_getRestBaseUrl()+"/workloads", params=request.args)

@app.route('/api/v3/workloads/download', methods=['GET'])
def downloadWorkloads():
    response = _genericGet(_getRestBaseUrl()+"/workloads/download", params=request.args)
    workload_name = "all_workloads"
    response.headers["Content-Disposition"] = "attachment; filename="+workload_name+".json"
    response.headers["Content-Type"] = "application/json"
    return response

@app.route('/api/v3/workloads/<workload_id>', methods=['GET'])
def getWorkload(workload_id):
    return _genericGet(_getRestBaseUrl()+"/workloads/"+workload_id, params=request.args)

def _get_workload_name(data):
    js = json.loads(data)
    wln = "generic_workload"
    tln = "generic_tool"
    if "workload_name" in js:
        wln = js["workload_name"]
    if "tool_name" in js:
        tln = js["tool_name"]
    return (tln+"-"+wln).replace(" ", "_").lower()

@app.route('/api/v3/workloads/<workload_id>/download', methods=['GET'])
def downloadWorkload(workload_id):
    response = _genericGet(_getRestBaseUrl()+"/workloads/"+workload_id+"/download", params=request.args)
    workload_name = _get_workload_name(response.data)
    response.headers["Content-Disposition"] = "attachment; filename="+workload_name+".json"
    response.headers["Content-Type"] = "application/json"
    return response

@app.route('/api/v3/workloads', methods=['POST'])
def addWorkload():
    return _genericPost(_getRestBaseUrl()+"/workloads/", request.data)

@app.route('/api/v3/workloads/<workload_id>', methods=['DELETE'])
def deleteWorkload(workload_id):
    return _genericDelete(_getRestBaseUrl()+"/workloads/"+workload_id)

@app.route('/api/v3/workloads/<workload_id>', methods=['PUT'])
def updateWorkload(workload_id):
    return _genericPut(_getRestBaseUrl()+"/workloads/"+workload_id, request.data)

@app.route('/api/v3/workloads/<workload_id>/user_actions')
def getWorkloadActions(workload_id):
    return _genericGet(_getRestBaseUrl()+"/workloads/"+workload_id+"/user_actions")

################## SCHEDULE OPERATIONS ###################

@app.route('/api/v3/schedules', methods=['GET'])
def listSchedules():
    return _genericGet(_getRestBaseUrl()+"/schedules")

@app.route('/api/v3/schedules/<schedule_id>', methods=['GET'])
def getSchedule(schedule_id):
    return _genericGet(_getRestBaseUrl()+"/schedules/"+schedule_id)

@app.route('/api/v3/schedules', methods=['POST'])
def addSchedule():
    return _genericPost(_getRestBaseUrl()+"/schedules/", request.data)

@app.route('/api/v3/schedules/<schedule_id>', methods=['DELETE'])
def deleteSchedule(schedule_id):
    return _genericDelete(_getRestBaseUrl()+"/schedules/"+schedule_id)

@app.route('/api/v3/schedules/<schedule_id>', methods=['PUT'])
def updateSchedule(schedule_id):
    return _genericPut(_getRestBaseUrl()+"/schedules/"+schedule_id, request.data)

@app.route('/api/v3/schedules/<schedule_id>/user_actions')
def getScheduleActions(schedule_id):
    return _genericGet(_getRestBaseUrl()+"/schedules/"+schedule_id+"/user_actions")

################## PROFILE OPERATIONS ###################

@app.route('/api/v3/private/profile')
def getLoggedProfile():
    if _getLoggedUser():
        user_session = UserSession(flask.session)
        out = {}
        ui = user_session.userinfo
        if "given_name" in ui:
            out["given_name"] = ui["given_name"]
        if "family_name" in ui:
            out["family_name"] = ui["family_name"]
        if "name" in ui:
            out["name"] = ui["name"]
        if "preferred_username" in ui:
            out["preferred_username"] = ui["preferred_username"]
        if "email" in ui:
            gravatar = _getGravatar(ui["email"])
            if gravatar:
                out["gravatar"] = gravatar
        return out
    else:
        return Response("not found", status=404, mimetype='plain/text')

def _getGravatar(email):
    hash = email
    hash = hashlib.md5(email.encode('utf-8')).hexdigest()
    params = {"d": "404"}
    res = requests.get(os.path.join("https://www.gravatar.com/avatar", hash), params=params)
    if res.status_code == 200:
        return os.path.join("https://www.gravatar.com/avatar", hash)
    else:
        return None
                       
@app.route('/api/v3/private/authenticated')
def isAutheticated():
    if _getLoggedUser():
        return jsonify({"authenticated":True})
    else:
        return jsonify({"authenticated":False})
        

@auth.error_view
def error(error=None, error_description=None):
    return jsonify({'error': error, 'message': error_description})

@app.after_request
def response_logging(response):
    logging.info("after request")
    logging.info(app.after_request_funcs)
    logging.info(response.headers)
    return response

def main():
    signal.signal(signal.SIGINT, on_exit)

    logging.basicConfig(
        level=logging.DEBUG,
        stream=sys.stdout)

    parser = argparse.ArgumentParser(prog='benchsuite-rest')
    parser.add_argument('--dump-specs', action='store_true',
                        help='dumps the Swagger specification')
    parser.add_argument('--listen', '-l', type=str,
                        help='set the listening host and port (e.g. 0.0.0.0:80). '
                             'If not specified, the default is 127.0.0.1:5000')


    # parse the arguments. They are taken both from the command line and
    # the BENCHSUITE_REST_OPTS environment variable
    env_opts = os.getenv('BENCHSUITE_REST_OPTS', '').split()
    args = parser.parse_args(sys.argv[1:] + env_opts)

    if args.dump_specs:
        print(generate_swagger_specs())
        sys.exit(0)

#    host = '127.0.0.1'
    host = '0.0.0.0'
    port = 5000
    
    if os.getenv("BENCHSUITE_API_PORT")!=None:
        port = os.getenv("BENCHSUITE_API_PORT")

    if args.listen:
        if ':' in args.listen:
            t = args.listen.rsplit(':', 1)
            host = t[0]
            port = int(t[1])
        else:
            print('ERROR: wrong format for listening address. '
                  'Must be in the format <host>:<port>')
            sys.exit(1)

    while True:
        try:
            auth.init_app(app)
            break
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout) as e:
            print(e)
            print("retrying in 30 secs...")
            time.sleep(30)

#    print('Using internal server. Not use this in production!!!')
#    app.run(host=host, port=port, debug=True)


    cherrypy.tree.graft(app.wsgi_app, '/')
    cherrypy.config.update({'server.socket_host': host,
                        'server.socket_port': port,
                        'engine.autoreload.on': True
                        })

    cherrypy.engine.start()
    cherrypy.engine.block()



if __name__ == '__main__':
    main()